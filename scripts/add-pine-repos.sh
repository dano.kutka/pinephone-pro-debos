#!/bin/sh

# Work around resolver failure in debos' fakemachine
mv /etc/resolv.conf /etc/resolv2.conf
echo "nameserver 1.1.1.1" > /etc/resolv.conf

export DEBIAN_FRONTEND=noninteractive
export DEBCONF_NONINTERACTIVE_SEEN=true

echo "deb http://repo.ubports.com/ focal_-_pine main" >> /etc/apt/sources.list.d/ubports.list

echo "" >> /etc/apt/preferences.d/ubports.pref
echo "Package: *" >> /etc/apt/preferences.d/ubports.pref
echo "Pin: origin repo.ubports.com" >> /etc/apt/preferences.d/ubports.pref
echo "Pin: release o=UBports,a=focal_-_pine" >> /etc/apt/preferences.d/ubports.pref
echo "Pin-Priority: 2001" >> /etc/apt/preferences.d/ubports.pref

apt-get update
apt-get install libpython3.8 -y

if [ -n "$APT_PROXY" ]; then
    alias apt-get="apt-get -o Acquire::http::Proxy='$APT_PROXY'"
fi

# Undo changes to work around debos fakemachine resolver
rm /etc/resolv.conf
mv /etc/resolv2.conf /etc/resolv.conf
