**PinePhone (non-Pro) users:** make sure you have followed the first section of steps at https://ubports.com/blog/ubports-news-1/post/pinephone-and-pinephone-pro-3889 before installing!

**Strongly Recommended Modem Firmware:**
- [Biktorgj's Open Modem Firmware v0.7.2](https://github.com/the-modem-distro/pinephone_modem_sdk/releases/tag/0.7.2)
  - ADSP Version 30.006.30.006

##### [Help Guide/Wiki](https://gitlab.com/ook37/pinephone-pro-debos/-/wikis/Help-Guide)

### PinePhone (Pro) + PineTab/2: Ubuntu Touch 20.04 Port
_features with a star are for phones only_

| Currently supported features: | Features partially working: | Features currently not working: |
|-------------------------------|-----------------------------|---------------------------------|
|  Boot into full UI            |  SSH                        |  Cameras (front and back)       |
|  Touchscreen                  |  Wired external monitor     |  Flashlight/torch               |
|  On-Screen Keyboard           |  Accelerometer/gyroscope    |  GPS                            |
|  SD card detection/access     |                             |  Proximity sensor               |
|  Online charging              |                             |                                 |
|  RTC time                     |                             |  
|  Shutdown/reboot              |                             |  
|  Wifi                         |                             
|  Bluetooth (FTP, Audio) _(PineTab2 needs port)_      |
|  Flight Mode                  |
|  Apparmor patches             |
|  PinePhone Keyboard*          |
|  Video playback               | 
|  Manual brightness            |
|  Notification LED*            |
|  Waydroid (mainline)          |
|  UART/serial console          |
|  Modem (Internet, SMS)*       |
|  On-device audio              |
|  Charging indicator LED*      |
|  Ethernet                     |

### Debos build recipes

This uses debos to build (sdcard) images for devices

Supported devices:
 - Pine64
    - PinePhone Don't be evil development kit
    - PinePhone Developer Batch (1.0)
    - PinePhone Braveheart (1.1)
    - PinePhone (1.2)
    - PinePhone Pro
    - PineTab
    - PineTab2

**NOTE:** Tow-Boot is ***required*** for PinePhone + Pro!

To build images locally, run:
```
./debos-docker -m 5G {device}.yaml
```

NOTE: this uses 5G of ram, you might be able to decreases this, but may run into
space issues if you do.

You will need docker installed and working.

If this is not working...

Prerequisites
-------------

* enabled virtualization:

  - check if you have `kvm` device node:

    ```
    ls /dev/kvm
    ```

  If not, enable virtualization in BIOS and check again.

  - add your user to kvm group:

    ```
    usermod -a -G kvm <your-user-name>
    ```

   Visit [linux-kvm FAQ](https://www.linux-kvm.org/page/FAQ) for more informations.

* `binfmt_misc` module loaded on the host machine:

```
modprobe binfmt_misc
```

Pull Docker image
-----------------

```
docker pull jbbgameich/debos-docker
```

Running
-------

```
./run.sh DEBOS_PARAMETERS
```

> Visit [debos repo](https://github.com/go-debos/debos) for tool usage

For easy access from any directory:

```
ln -s $(readlink -f run.sh) ~/bin/debos-docker
debos-docker DEBOS_PARAMETERS

git clone https://gitlab.com/ook37/pinephone-pro-debos.git
cd pinephone-pro-debos
debos-docker -m 5G <device>.yaml
For PinePhone + Pro this means 
debos-docker -m 5G pinephone-unified.yaml
